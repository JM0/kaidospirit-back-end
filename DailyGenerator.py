import json
import random 
from random import randint

dailyJSON = {
    "ChallengeName": "Daily Challenge",
    "ChallengeParameters": [
        {
            "TrackID": randint(0, 10),
            "CarID": randint(0, 10)
        }
    ],
    "WeatherParameters": [
        {
            "Day": random.choice([True, False]),
            "Rain": random.choice([True, False]),
            "Season": randint(0, 3)
        }
    ]
}


daily_JSON_path = "./JSON/daily.json"

def generateDaily():
    with open(daily_JSON_path, 'r+') as f:
        json_data = json.load(f)
        json_data = dailyJSON
        f.seek(0)
        f.write(json.dumps(json_data, indent=4))
        f.truncate()
        f.close

if __name__ == "__main__":
    generateDaily()