from flask import Flask
from flask_restful import Resource, Api
import json

#Flask RESTful application name 
app = Flask(__name__)
api = Api(app)

#Defining paths to generated JSON files
daily_file_path = "./JSON/daily.json"
weekly_file_path = "./JSON/weekly.json"
monthly_file_path = "./JSON/monthly.json"

#Functions to open the generated JSON files
#JSON Files are generated useing generator.py
def getDaily(daily):
    with open(daily_file_path, 'r') as f:
        dailyTemp = f.read()
    daily = json.loads(dailyTemp)
    return daily
    f.close()

def getWeekly(weekly):
    with open(weekly_file_path, 'r') as f:
        weeklyTemp = f.read()
    weekly = json.loads(weeklyTemp)
    return weekly
    f.close()

def getMonthly(monthly):
    with open(monthly_file_path, 'r') as f:
        monthlyTemp = f.read()
    monthly = json.loads(monthlyTemp)
    return monthly
    f.close()

#API resources that load the JSON objects
class Daily(Resource):
    def get(self):
        daily = {}
        return getDaily(daily)

class Weekly(Resource):
    def get(self):
        weekly = {}
        return getWeekly(weekly)

class Monthly(Resource):
    def get(self):
        monthly = {}
        return getMonthly(monthly)

#API endpoints 
api.add_resource(Daily, '/daily')
api.add_resource(Weekly, '/weekly')
api.add_resource(Monthly, '/monthly')

#Runs main server class 
if __name__ == '__main__':
    app.run(debug=True)